import requests
import json
import base64

# URL para qual será feita a requisição
URL = "http://localhost:8082/pdf/v1/prescricao"
# Token de autenticação gerado no BRy Cloud
TOKEN = 'tokenDeAutenticação'

def adiciona_metadados():
    # Documento que será assinado
    arquivo = open("./arquivos/teste.pdf", 'rb').read()

    arquivos = []
    arquivos.append(('prescricao[0][documento]', arquivo))

    # Dados relacionados ao profissional que irá assinar o documento
    dados = {
        'prescricao[0][tipo]': 'MEDICAMENTO',
        'profissional': 'MEDICO',
        'numeroRegistro': '123456',
        'UF': 'SC',
        'especialidade': 'ONCOLOGISTA'
    }
    header = {'Authorization': TOKEN}

    # Envio da requisição para a API BRy 
    response = requests.post(URL, data = dados, headers=header, files=arquivos)
    # O retorno será o arquivo com os MetaDados relacionados à prescrição, que agora precisará ser assinado.
    if response.status_code == 200:
        arquivo_com_metadados = open('./arquivoComMetadados.pdf', 'wb')
        arquivo_com_metadados.write(response.content)
        arquivo_com_metadados.close()
    else: 
        print(response.text)

adiciona_metadados()