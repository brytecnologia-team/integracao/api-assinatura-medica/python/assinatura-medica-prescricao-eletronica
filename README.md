# Geração de Assinatura Médica

Este é exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia Python.

Este exemplo apresenta os passos necessários para a geração de metadados referentes ao médico na prescrição médica.

  - Passo 1: Criação do formulário com o documento que será assinado.
  - Passo 2: Recebimento do documento com os metadados.
  - Passo 3: O documento recebido deverá ser enviado para uma assinatura do tipo PDF/PADES.

### Assinatura PDF

Após ter os metadados inseridos, o documento deverá ser enviado novamente ao HUB, para que seja realizada de fato a assinatura. Um Exemplo de como realizar a assinatura com certificado instalado no navegador se encontra [aqui](https://gitlab.com/brytecnologia-team/integracao/api-assinatura/python/geracao-de-assinatura-pdf-com-certificado-no-navegador-extension) e um exemplo com certificado armazenado em nuvem se encontra [aqui](https://gitlab.com/brytecnologia-team/integracao/api-assinatura/python/geracao-de-assinatura-pdf-com-certificado-na-nuvem-kms).

### Tech

O exemplo utiliza das bibliotecas Python abaixo:
* [Python3] - Python 3.8
* [Requests] - Elegant and simple HTTP library for Python, built for human beings.

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Esse token de acesso pode ser obtido através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

| Variável  |                   Descrição                   | Arquivo de Configuração |  
| --------- | --------------------------------------------- | ----------------------- |
| TOKEN | Access Token para o consumo do serviço (JWT). |     assinatura_medica.py    | 

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

### Uso

É necessário ter o [Python3] instalado na sua máquina para a execução do projeto.

Para execução da aplicação de exemplo, importe o projeto em sua IDE de preferência.

Comandos:

Executar programa:

    - python3 assinatura_medica.py

  [Python3]: <https://www.python.org/>
  [Requests]: <https://requests.readthedocs.io/en/master/>

